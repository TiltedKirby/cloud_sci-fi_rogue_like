﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour 
{
	public float shotspeed;

	private float currentTime;

	void Start () 
	{
	
	}

	void Update () 
	{
		currentTime += Time.deltaTime;
		transform.position += -transform.up * Time.deltaTime * shotspeed;
		if (currentTime > 15) {
			DestroyObject (this.gameObject);
		}
	}
}
