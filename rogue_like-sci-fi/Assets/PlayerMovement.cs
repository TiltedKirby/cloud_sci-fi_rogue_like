﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
	public float speed;
	public GameObject head;

	private float speedTemp = 0f;
	private float angle;
	private Vector3 mousePos;
	private Vector3 direction = new Vector3 (0, 0, 0);

	void Start () 
	{
		
	}

	void Update () 
	{
		Move ();
		Rotate ();
	}

	private void Rotate ()
	{
		mousePos = Input.mousePosition;
		mousePos.z = 10;
		Vector3 targetPos = Camera.main.WorldToScreenPoint (transform.position + new Vector3 (0, 0.5f, 0));
		mousePos.x = mousePos.x - targetPos.x;
		mousePos.y = mousePos.y - targetPos.y;
		angle = Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg;
		head.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle + 90f));
	}

	private void Move ()
	{		
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.D)) {
			speedTemp = Mathf.Lerp (speedTemp, speed, Time.deltaTime * 10);
			if (Input.GetKey (KeyCode.W)) {
				if (Input.GetKey (KeyCode.A)) {
					direction = Vector3.Lerp (direction, new Vector3 (-0.75f, 0.75f, 0), Time.deltaTime * 4);
				} else if (Input.GetKey (KeyCode.D)) {
					direction = Vector3.Lerp (direction, new Vector3 (0.75f, 0.75f, 0), Time.deltaTime * 4);
				} else {
					direction = Vector3.Lerp (direction, Vector3.up, Time.deltaTime * 6);
				}
			}
			if (Input.GetKey (KeyCode.A)) {
				if (Input.GetKey (KeyCode.W)) {
					direction = Vector3.Lerp (direction, new Vector3 (-0.75f, 0.75f, 0), Time.deltaTime * 4);
				} else if (Input.GetKey (KeyCode.S)) {
					direction =Vector3.Lerp (direction, new Vector3 (-0.75f, -0.75f, 0), Time.deltaTime * 4);
				} else {
					direction = Vector3.Lerp (direction, -Vector3.right, Time.deltaTime * 6);
				}
			}
			if (Input.GetKey (KeyCode.S)) {
				if (Input.GetKey (KeyCode.A)) {
					direction = Vector3.Lerp (direction, new Vector3 (-0.75f, -0.75f, 0), Time.deltaTime * 4);
				} else if (Input.GetKey (KeyCode.D)) {
					direction = Vector3.Lerp (direction, new Vector3 (0.75f, -0.75f, 0), Time.deltaTime * 4);
				} else {
					direction = Vector3.Lerp (direction, -Vector3.up, Time.deltaTime * 6);
				}
			}
			if (Input.GetKey (KeyCode.D)) {
				if (Input.GetKey (KeyCode.W)) {
					direction = Vector3.Lerp (direction, new Vector3 (0.75f, 0.75f, 0), Time.deltaTime * 4);
				} else if (Input.GetKey (KeyCode.S)) {
					direction = Vector3.Lerp (direction, new Vector3 (0.75f, -0.75f, 0), Time.deltaTime * 4);
				} else {
					direction = Vector3.Lerp (direction, Vector3.right, Time.deltaTime * 6);
				}
			}
		} else {
			speedTemp = Mathf.Lerp (speedTemp, 0, Time.deltaTime * 10f);
			if (speedTemp < 0.1f) {
				direction = Vector3.zero;
			}
		}
		transform.Translate (direction * speedTemp * Time.deltaTime);
	}
}
