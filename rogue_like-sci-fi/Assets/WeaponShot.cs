﻿using UnityEngine;
using System.Collections;

public class WeaponShot : MonoBehaviour 
{
	public GameObject amunition;
	public float shotCooldown;
	public GameObject head;
	public Transform weapon;

	private Vector3 weaponPos;
	private float currentShotCooldown;

	void Start () 
	{
		currentShotCooldown = shotCooldown;
	}

	void Update () 
	{
		Shoot ();
		transform.rotation = head.transform.rotation;
		weaponPos = weapon.position - weapon.transform.up * 0.15f;
	}

	private void Shoot ()
	{
		currentShotCooldown += Time.deltaTime;
		//TODO: Make it possible to change weapons/ amunition
		if (Input.GetMouseButton (0) && currentShotCooldown > shotCooldown) {
			Instantiate (amunition, weaponPos, transform.rotation);
			currentShotCooldown = 0;
		}
	}
}
